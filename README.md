# mailchimp-clojure-tutorial

This is a toy example of a simple frontend account creation form. It is built utilizing ClojureScript and Reagent. A couple features/principles displayed:
*  Creating generalized functions for UI components and using those as utils to cleanly implement specific instances.
*  Using Atoms to handle UI refreshes on content change, such as for password rule validation and the associated hints displayed
## Building and Testing Yourself
To build:
1.  Clone this repo.
2.  Cd in the project directory.
3.  Ensure you have Leiningen installed, and run

        lein ring server
        lein figwheel
4.  View the example at localhost:3000 in your browser.
