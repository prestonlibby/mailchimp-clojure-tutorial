(ns mailchimp-clojure.prod
  (:require [mailchimp-clojure.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
